import  styled  from 'styled-components'

export const Tank = styled.div((right, top, deg)=> ({
    position: 'absolute',
    top: `${top}px`,
    right: `${right}px`,
    transform: `rotate(${deg})`,
    width: '20px',
    height: '20px',
    backgroundColor: 'back',
}));