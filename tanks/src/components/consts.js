export const initialTanks = [
    {
        id: 0,
            coordinates: {
            x: 0,
            y: 0,
            },
        rotation: 0,
    },
    {
        id: 1,
        coordinates: {
            x: 125,
            y: 125,
        },
        rotation: 90,
    },
    {
        id: 2,
        coordinates: {
            x: 300,
            y: 300,
        },
        rotation: 180,
    },
    {
        id: 3,
        coordinates: {
            x: 600,
            y: 600,
        },
        rotation: 270,
    },

]