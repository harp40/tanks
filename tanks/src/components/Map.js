import React from "react";
import {useKeyPress} from './Hooks'
import {initialTanks} from './consts'
import {Tank} from './tank.style'


export const Map = () => {
    const up = useKeyPress('w');
    const down = useKeyPress('s');
    const left = useKeyPress('a');
    const right = useKeyPress('d');
    const shot = useKeyPress('0')
    const [tanks, setTanks] = React.useState(initialTanks)

    return(
        <div className='map-container' style={{position: 'relative'}}>
            {
                tanks.map( tank => {
                    return (
                        <Tank key={tank.id} right={tank.coordinates.x} top={tank.coordinates.y} deg={tank.rotation} />
                    )
                })
            }
            {up && 'up'}
            {down && 'down'}
            {left && 'left'}
            {right && 'right'}
            {shot && 'shot'}
        </div>
    )
}